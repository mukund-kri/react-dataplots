const API_SERVER = 'http://localhost:8080';

export default {
    PROBLEM1_URL: `${API_SERVER}/problem1`,
    PROBLEM2_URL: `${API_SERVER}/problem2`,
    PROBLEM3_URL: `${API_SERVER}/problem3`,
    PROBLEM4_URL: `${API_SERVER}/problem4`,
}
