import React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';

import _ from 'lodash';

import '../Control.css'

const options = _.range(2000, 2018).map((year) => {
    return { value: year, label: `${year}` }
});

export default ({ selectedYear, setSelectedYear }) => {


    const handleYearChange = (event) => {
        setSelectedYear(event.target.value);
    };


    return (
        <div className='chart-control'>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="selected-year-label">Start Year</InputLabel>
                <Select
                    labelId="selected-year-label"
                    id="selected-year"
                    value={selectedYear}
                    label="Start Year"
                    onChange={handleYearChange}
                >
                    {options.map((option) => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>

        </div>
    )
}