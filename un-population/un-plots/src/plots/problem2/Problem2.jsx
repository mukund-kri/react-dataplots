import React from "react";

import Plot from "./Plot";
import Control from "./Control";
import Config from "../../Config";


export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Selected Year controls
    const [selectedYear, setSelectedYear] = React.useState(2010);

    // Get JSON data from server
    React.useEffect(() => {
        fetch(Config.PROBLEM2_URL + `?selectedYear=${selectedYear}`)
            .then(res => res.json())
            .then(json => setData(json))
    }, [selectedYear]);

    // display the json
    return (
        <>
            <h1>ASEAN population for year {selectedYear} </h1>
            <Control selectedYear={selectedYear} setSelectedYear={setSelectedYear} />
            <Plot data={data} />
        </>
    )
}