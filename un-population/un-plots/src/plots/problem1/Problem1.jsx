import React from "react";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import Config from "../../Config";
import NetworkError from "../../errors/NetworkError";
import Control from "./Control";

export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Error message
    const [error, setError] = React.useState(false);

    // Date range controls
    const [dateRange, setDateRange] = React.useState({ startYear: 2010, endYear: 2015 });

    // Load json on page load
    React.useEffect(() => {
        fetch(Config.PROBLEM1_URL + `?startYear=${dateRange.startYear}&endYear=${dateRange.endYear}`)
            .then(res => res.json())
            .then(json => setData(json))
            .then(setError(false))
            // Show error message if fetch fails
            .catch(err => {
                console.error("Error loading data: " + err);
                setError(true);
            })
    }, [dateRange]);

    // display the json
    if (error) {
        return <NetworkError />
    } else {
        return (
            <ResponsiveContainer width="100%" height={600}>
                <h1>India population Growth</h1>
                <Control dateRange={dateRange} setDateRange={setDateRange} />
                <LineChart
                    title="Companies by Authorized Capital"
                    data={data}
                    margin={{
                        top: 50, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="year" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line dataKey="population" type="monotone" stroke="#8884d8" />
                </LineChart>
            </ResponsiveContainer>
        )
    }
};