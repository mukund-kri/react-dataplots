import React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import _ from 'lodash';


const yearOptions = _.range(2000, 2018).map((year) => {
    return { value: year, label: `${year}` }
});


export default ({ dateRange, setDateRange }) => {

    const handleStartYearChange = (event) => {
        let newDateRange = { ...dateRange, startYear: event.target.value };
        setDateRange(newDateRange);
    };

    const handleEndYearChange = (event) => {
        let newDateRange = { ...dateRange, endYear: event.target.value };
        setDateRange(newDateRange);
    }

    return (
        <div className='chart-control'>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="year-label">From Year:</InputLabel>
                <Select
                    labelId="year-label"
                    id="startYear"
                    value={dateRange.startYear}
                    label="From Year"
                    onChange={handleStartYearChange}
                >
                    {yearOptions.map((option) => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>

            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="top-label">To Year:</InputLabel>
                <Select
                    labelId="top-label"
                    id="endYear"
                    value={dateRange.endYear}
                    label="To Year"
                    onChange={handleEndYearChange}
                >
                    {yearOptions.map((option) => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    )
}