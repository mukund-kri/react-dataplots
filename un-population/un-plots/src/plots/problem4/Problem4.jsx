import React from "react";

import Plot from "./Plot";
import Config from "../../Config";
import Control from "../problem1/Control";


export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Date range controls
    const [dateRange, setDateRange] = React.useState({ startYear: 2010, endYear: 2015 });

    // PBA's to render
    const [pbas, setPbas] = React.useState([]);

    // Get JSON data from server
    React.useEffect(() => {
        fetch(Config.PROBLEM4_URL + `?startYear=${dateRange.startYear}&endYear=${dateRange.endYear}`)
            .then(res => res.json())
            .then(json => setData(json))
    }, [dateRange]);

    console.log("Here");

    // display the json
    return (
        <>
            <h1>ASEAN population over a range of years</h1>
            <Control dateRange={dateRange} setDateRange={setDateRange} />
            <Plot data={data} />
        </>
    );

}