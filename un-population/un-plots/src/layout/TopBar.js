// Tob bar for the app
import React from 'react';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';

import MenuIcon from '@mui/icons-material/Menu';
import { Link } from 'react-router-dom';

function TopBar() {
    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="menu"

                    sx={{ mr: 2 }}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    UN Population Analysis
                </Typography>

                <Typography variant="h6" component="div" sx={{ mr: 2 }}>
                    <Link to="/problem1">Plot 1</Link>
                </Typography>
                <Typography variant="h6" component="div" sx={{ mr: 2 }}>
                    <Link to="/problem2">Plot 2</Link>
                </Typography>
                <Typography variant="h6" component="div" sx={{ mr: 2 }}>
                    <Link to="/problem3">Plot 3</Link>
                </Typography>
                <Typography variant="h6" component="div" sx={{ mr: 2 }}>
                    <Link to="/problem4">Plot 4</Link>
                </Typography>
            </Toolbar>
        </AppBar>
    );
}

export default TopBar;