# Plotting Data with React

This repo contains code that I use in my boot-camps to teach fullstack programming. Only
the frontend code is present. The idea is the students write the backend code in
any language / framework that will drive this frontend.

## Projects

I have 3 projects that I typically use in my boot-camps ...

1. Company Master Data (from Government of India open data)
2. IPL (Indian primer League) ball by ball data set
3. UN country wise, development data

## Stack

- React 18.x
- recharts


## Running this code

```shell
> npm start

```

## TODO

1. Dockerize this
2. Finish the IPL and UN datasets
