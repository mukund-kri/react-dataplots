/*
 * Generate json for "company master data" for development of the recharts version
 * of the company master data visualization.
 */

const fs = require('fs');
const csv = require('csv-parser');


const categories = ['< 1L', '1L to 10L', '10L to 1Cr', '1Cr to 10Cr', '> 10Cr'];
const counts = categories.reduce((acc, cur) => {
    acc[cur] = 0;
    return acc;
}, {});

const countsPerYear = {};
const countsPerPBA = {};
const countsPerPBAPerYear = {};

const csvStream = fs.createReadStream('mca.csv').pipe(csv());

// PROBLEM 1
csvStream.on('data', (line) => {
    if (line.AUTHORIZED_CAP <= 100000) {
        counts['< 1L'] += 1;
    } else if (line.AUTHORIZED_CAP > 100000 && line.AUTHORIZED_CAP <= 1000000) {
        counts['1L to 10L'] += 1;
    } else if (line.AUTHORIZED_CAP > 1000000 && line.AUTHORIZED_CAP <= 10000000) {
        counts['10L to 1Cr'] += 1;
    } else if (line.AUTHORIZED_CAP > 10000000 && line.AUTHORIZED_CAP <= 100000000) {
        counts['1Cr to 10Cr'] += 1;
    } else {
        counts['> 10Cr'] += 1;
    }
});

// COMPUTE THE FINAL JSON AND WRITE TO FILE
csvStream.on('ender', () => {
    let plotData = [];

    categories.forEach((cat) => {
        plotData.push({
            name: cat,
            registrations: counts[cat]
        });
    });

    fs.writeFileSync('problem1.json', JSON.stringify(plotData));
});


// PROBLEM 2
csvStream.on('data', (line) => {
    year = line.DATE_OF_REGISTRATION.split('-')[2];
    if (year >= '2000' && year <= '2019') {
        countsPerYear[year] = countsPerYear[year] || 0;
        countsPerYear[year] += 1;
    }
});

// COMPUTE THE FINAL JSON AND WRITE TO FILE
csvStream.on('ender', () => {
    let plotData = [];

    Object.keys(countsPerYear).forEach((year) => {
        plotData.push({
            year: year,
            registrations: countsPerYear[year]
        });
    });

    fs.writeFileSync('problem2.json', JSON.stringify(plotData));
});


// PROBLEM 3
csvStream.on('data', (line) => {
    year = line.DATE_OF_REGISTRATION.split('-')[2];
    pba = line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;
    if (year == '2015') {
        countsPerPBA[pba] = countsPerPBA[pba] || 0;
        countsPerPBA[pba] += 1;
    }
});

// COMPUTE THE FINAL JSON AND WRITE TO FILE
csvStream.on('ender', () => {
    console.log(countsPerPBA);

    let plotData = [];

    Object.keys(countsPerPBA).forEach((pba) => {
        plotData.push({
            pba: pba,
            registrations: countsPerPBA[pba]
        });
    });

    fs.writeFileSync('problem3.json', JSON.stringify(plotData));
});


// PROBLEM 4
csvStream.on('data', (line) => {
    year = line.DATE_OF_REGISTRATION.split('-')[2];
    pba = line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;
    if (year >= '2000' && year <= '2019') {
        countsPerPBAPerYear[pba] = countsPerPBAPerYear[pba] || {};
        countsPerPBAPerYear[pba][year] = countsPerPBAPerYear[pba][year] || 0;
        countsPerPBAPerYear[pba][year] += 1;
    }
});

// COMPUTE THE FINAL JSON AND WRITE TO FILE
csvStream.on('end', () => {
    plotData = [];

    Object.keys(countsPerPBAPerYear).forEach((pba) => {
        let pbaData = { "pba": pba };
        Object.keys(countsPerPBAPerYear[pba]).forEach((year) => {
            pbaData[year] = countsPerPBAPerYear[pba][year];
        });
        plotData.push(pbaData);
    });

    fs.writeFileSync('problem4.json', JSON.stringify(plotData));
});
