import TobBar from './layout/TopBar'; // Import the TopBar component
import {
  Routes,
  Route,
  Link,
  Outlet
} from "react-router-dom";

import Problem1 from './plots/problem1/Problem1'
import Problem2 from './plots/problem2/Problem2'
import Problem3 from './plots/problem3/Problem3'
import Problem4 from './plots/problem4/Problem4'

import './App.css';


function App() {
  return (
    <div className="App">

      <TobBar />
      <Routes>
        <Route path="/problem1" element={<Problem1 />} />
        <Route path="/problem2" element={<Problem2 />} />
        <Route path="/problem3" element={<Problem3 />} />
        <Route path="/problem4" element={<Problem4 />} />
      </Routes>
    </div>
  );
}

export default App;
