/// Display this widget when a network error occurs.

import React from "react";

const NetworkError = () => {
    return (
        <div>
            <h1>Network Error</h1>
            <p>There was a problem loading the.
                Do the following ...</p>
            <ol>
                <li>Check if api server is running</li>
                <li>Check CORS</li>
            </ol>

        </div>
    );
};

export default NetworkError;