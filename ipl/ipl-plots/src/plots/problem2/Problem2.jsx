import React from "react";

import Plot from "./Plot";
import Config from "../../Config";


export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Date range controls
    const [dateRange, setDateRange] = React.useState({ startYear: 2010, endYear: 2015 });

    // Get JSON data from server
    React.useEffect(() => {
        fetch(Config.PROBLEM2_URL + `?startYear=${dateRange.startYear}&endYear=${dateRange.endYear}`)
            .then(res => res.json())
            .then(json => setData(json))
    }, [dateRange]);

    // display the json
    return (
        <>
            <h1>Top Batsmen for RCB</h1>
            <Plot data={data} />
        </>
    )
}