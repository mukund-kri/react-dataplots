import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

export default ({ data }) => {
    return (
        <ResponsiveContainer width="100%" height={600}>
            <BarChart
                data={data}
                margin={{
                    top: 50, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="batsman" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="runs" />
            </BarChart>
        </ResponsiveContainer>
    )
};