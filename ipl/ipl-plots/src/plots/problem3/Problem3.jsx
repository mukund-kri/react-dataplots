import React from "react";

import Plot from "./Plot";
import Config from "../../Config";


export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Year of analysis
    const [year, setYear] = React.useState(2010);

    // Number o pbas to show
    const [top, setTop] = React.useState(5);

    // Get JSON data from server
    React.useEffect(() => {
        fetch(Config.PROBLEM3_URL)
            .then(res => res.json())
            .then(json => setData(json))
    }, [year, top]);

    // display the json
    return (
        <>
            <h1>Umpires by country of origin</h1>
            <Plot data={data} />
        </>
    )
}