import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';

import Config from "../../Config";
import NetworkError from "../../errors/NetworkError";
import { tickFormatter } from "../../errors/utils/utils";

export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Error message
    const [error, setError] = React.useState(false);

    // Load json on page load
    React.useEffect(() => {
        fetch(Config.PROBLEM1_URL)
            .then(res => res.json())
            .then(json => setData(json))
            .then(setError(false))
            // Show error message if fetch fails
            .catch(err => {
                console.error("Error loading data: " + err);
                setError(true);
            })
    }, []);

    // display the json
    if (error) {
        return <NetworkError />
    } else {
        return (
            <ResponsiveContainer width="100%" height={700}>
                <h1>Total Runs by Team</h1>
                <BarChart
                    title="Companies by Authorized Capital"
                    data={data}
                    margin={{
                        top: 50, right: 30, left: 80, bottom: 80,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="label" angle={35} tickFormatter={tickFormatter} dy={50}>
                        <Label value="Teams" position="insideBottom" dy={100} />
                    </XAxis>
                    <YAxis>
                        <Label value="Runs" angle={-90} position="insideLeft" style={{ textAnchor: 'middle' }} dx={-10} />
                    </YAxis>
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="count" fill="#8884d8" />
                </BarChart>
            </ResponsiveContainer>
        )
    }
};