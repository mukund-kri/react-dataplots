# React (Re-Charts) app for Maharashtra Company Data Visualization

This project contains the solution for the Maharashtra Company Data Visualization 
problem statement. The project is implemented using React and Re-Charts library.

## JSON Input

The following API is needed to get this project running ...

### Problem1: /problem1.json
Expected JSON

```json
[
    {
        "label": "< 1L",
        "count": 32927
    },
    {
        "label": "1L - 10L",
        "count": 86572
    },
    {
        "label": "10L - 1Cr",
        "count": 58929
    },
    {
        "label": "1Cr - 10Cr",
        "count": 18873
    },
    {
        "label": "> 10Cr",
        "count": 2406
    }
]
```

### Problem2: /problem2.json
Expected JSON

```json
[
    {
        "year": 2010,
        "registrations": 15390
    },
    {
        "year": 2011,
        "registrations": 14873
    },
    {
        "year": 2012,
        "registrations": 17926
    },
    {
        "year": 2013,
        "registrations": 9657
    },
    {
        "year": 2014,
        "registrations": 5403
    },
    {
        "year": 2015,
        "registrations": 4304
    }
]
```

### Problem3: /problem3.json
Expected JSON

```json
[
    {
        "pba": "Trading",
        "registrations": 8840
    },
    {
        "pba": "Business Services",
        "registrations": 2285
    },
    {
        "pba": "Real Estate and Renting",
        "registrations": 1052
    },
    {
        "pba": "Construction",
        "registrations": 1021
    },
    {
        "pba": "Manufacturing (Metals & Chemicals, and products thereof)",
        "registrations": 372
    }
]
```

### Problem4: /problem4.json
Expected JSON

```json
[
    {
        "2000": 131,
        "2001": 94,
        "2002": 110,
        "2003": 118,
        "2004": 211,
        "2005": 180,
        "2006": 178,
        "2007": 381,
        "2008": 406,
        "2009": 265,
        "2010": 396,
        "2011": 475,
        "2012": 471,
        "2013": 444,
        "2014": 465,
        "2015": 761,
        "2016": 732,
        "2017": 841,
        "2018": 264,
        "pba": "Agriculture and Allied Activities"
    },
    {
        "2000": 34,
        "2001": 11,
        "2002": 18,
        "2003": 33,
        "2004": 54,
        "2005": 79,
        "2006": 72,
        "2007": 85,
        "2008": 256,
        "2009": 126,
        "2010": 150,
        "2011": 165,
        "2012": 104,
        "2013": 106,
        "2014": 65,
        "2015": 59,
        "2016": 36,
        "2017": 54,
        "2018": 19,
        "pba": "Mining & Quarrying"
    }
]
```

The full JSON is found in the `sample-data` folder.

## Running the project

1. Install npm packages
```bash
npm install
```

2. Start the project
```bash
npm start
```

3. Open the browser and navigate to `http://localhost:3000/` to view the project.