import React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';

import _ from 'lodash';

import '../Control.css'

const options = _.range(2000, 2018).map((year) => {
    return { value: year, label: `${year}` }
});

export default ({ dateRange, setDateRange }) => {

    const [startYear, setStartYear] = React.useState(dateRange.startYear);
    const [endYear, setEndYear] = React.useState(dateRange.endYear);

    const handleStartYearChange = (event) => {
        setStartYear(event.target.value);
    };

    const handleEndYearChange = (event) => {
        console.log(startYear, endYear);
        setEndYear(event.target.value);
    };

    return (
        <div className='chart-control'>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="start-year-label">Start Year</InputLabel>
                <Select
                    labelId="start-year-label"
                    id="start-year"
                    value={startYear}
                    label="Start Year"
                    onChange={handleStartYearChange}
                >
                    {options.map((option) => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="end-year-label">End Year</InputLabel>
                <Select
                    labelId="end-year-label"
                    id="end-year"
                    value={endYear}
                    label="End Year"
                    onChange={handleEndYearChange}
                >
                    {options.map((option) => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <Button variant="contained" onClick={() => setDateRange({ startYear, endYear })}>
                    Re-Draw
                </Button>
            </FormControl>
        </div>
    )
}