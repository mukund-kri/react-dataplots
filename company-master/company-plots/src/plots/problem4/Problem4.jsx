import React from "react";

import Plot from "./Plot";
import Config from "../../Config";


export default () => {
    console.log(Config.PROBLEM4_URL);
    // Data for the chart
    const [data, setData] = React.useState(null);

    // PBA's to render
    const [pbas, setPbas] = React.useState([]);

    // Get JSON data from server
    React.useEffect(() => {
        fetch(Config.PROBLEM4_URL)
            .then(res => res.json())
            .then(json => setData(json))
    }, []);

    console.log(data);

    // display the json
    return <Plot data={data} />

}