import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import _ from "lodash";
import { tickFormatter } from "../../errors/utils/utils";

const barChartColors = [
    "#8884d8",
    "#82ca9d",
    "#ffc658",
    "#ff0000",
    "#00ff00",
    "#0000ff",
    "#ffff00",
    "#00ffff",
    "#ff00ff",
    "#000000",
    "#ffffff",
    "#ff8000",
    "#8000ff",
    "#0080ff",
    "#ff0080",
    "#00ff80",
    "#80ff00",
    "#ff0080",
    "#0080ff",
    "#ff8000",
];


export default ({ data }) => {
    return (

        <ResponsiveContainer width="100%" height={1200}>
            <BarChart
                data={data}
                margin={{
                    top: 50, right: 30, left: 20, bottom: 5,
                }}
            >

                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="pba" angle={45} height={500} interval={0} tickFormatter={tickFormatter} />
                <YAxis />
                <Tooltip />
                <Legend />

                {_.map(_.range(2000, 2010), (year, index) => {
                    return <Bar dataKey={year} fill={barChartColors[index]} />
                })}
            </BarChart>
        </ResponsiveContainer>
    )
};