import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import Config from "../../Config";
import NetworkError from "../../errors/NetworkError";

export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Error message
    const [error, setError] = React.useState(false);

    // Load json on page load
    React.useEffect(() => {
        fetch(Config.PROBLEM1_URL)
            .then(res => res.json())
            .then(json => setData(json))
            .then(setError(false))
            // Show error message if fetch fails
            .catch(err => {
                console.error("Error loading data: " + err);
                setError(true);
            })
    }, []);

    // display the json
    if (error) {
        return <NetworkError />
    } else {
        return (
            <ResponsiveContainer width="100%" height={600}>
                <h1>Companies by Authorized Capital</h1>
                <BarChart
                    title="Companies by Authorized Capital"
                    data={data}
                    margin={{
                        top: 50, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="label" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="count" fill="#8884d8" />
                </BarChart>
            </ResponsiveContainer>
        )
    }
};