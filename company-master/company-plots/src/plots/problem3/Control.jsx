/**
 * Controls for the problem 3 plot. 
 *  - It has a single select input for selecting the year under analysis.
 *  - TODO: The number of pbas to show in the plot.
 */

import React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import _ from 'lodash';


const options = _.range(2000, 2018).map((year) => {
    return { value: year, label: `${year}` }
});

const topOptions = _.range(3, 8).map((num) => {
    return { value: num, label: `${num}` }
});

export default ({ year, setYear, top, setTop }) => {

    const handleYearChange = (event) => {
        setYear(event.target.value);
    };

    const handleTopChange = (event) => {
        setTop(event.target.value);
    }

    return (
        <div className='chart-control'>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="year-label">Year</InputLabel>
                <Select
                    labelId="year-label"
                    id="year"
                    value={year}
                    label="Year"
                    onChange={handleYearChange}
                >
                    {options.map((option) => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>

            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="top-label">Top PBAs</InputLabel>
                <Select
                    labelId="top-label"
                    id="top"
                    value={top}
                    label="Top PBAs"
                    onChange={handleTopChange}
                >
                    {topOptions.map((option) => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    )
}