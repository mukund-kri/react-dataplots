import React from "react";

import Plot from "./Plot";
import Config from "../../Config";
import Control from "./Control";


export default () => {

    // Data for the chart
    const [data, setData] = React.useState(null);

    // Year of analysis
    const [year, setYear] = React.useState(2010);

    // Number o pbas to show
    const [top, setTop] = React.useState(5);

    // Get JSON data from server
    React.useEffect(() => {
        fetch(Config.PROBLEM3_URL + `?selectedYear=${year}&numOfPbas=${top}`)
            .then(res => res.json())
            .then(json => setData(json))
    }, [year, top]);

    // display the json
    return (
        <>
            <h1>Company registration by PBA</h1>
            <Control year={year} setYear={setYear} top={top} setTop={setTop} />
            <Plot data={data} />
        </>
    )
}