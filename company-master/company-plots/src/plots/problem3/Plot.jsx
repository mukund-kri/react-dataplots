import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { tickFormatter } from "../../errors/utils/utils";

export default ({ data }) => {
    return (
        <ResponsiveContainer width="100%" height={1000}>
            <BarChart

                data={data}
                margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="pba" angle={90} height={500} interval={0} tickFormatter={tickFormatter} />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="registrations" fill="#8884d8" />
            </BarChart>
        </ResponsiveContainer>
    )
};